package com.mxm.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 漫小猫
 * @since 2019-08-01
 */
@Data
@TableName("user")
public class Student extends Model<Student> {
    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	@TableField("stu_name")
	private String stuName;
	@TableField("stu_number")
	private String stuNumber;
	private Integer age;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}


}

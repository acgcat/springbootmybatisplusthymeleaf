package com.mxm.service;

import com.mxm.entity.Student;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 漫小猫
 * @since 2019-08-01
 */
public interface IStudentService extends IService<Student> {
	List<Student> selectStudentByStuName(String student);
}

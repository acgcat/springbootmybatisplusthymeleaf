package com.mxm.serviceImpl;

import com.mxm.entity.Student;
import com.mxm.mapper.StudentMapper;
import com.mxm.service.IStudentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 漫小猫
 * @since 2019-08-01
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {
    @Override
    public List<Student> selectStudentByStuName(String Student) {
        return this.baseMapper.selectStudentByStuName(Student);
    }
}

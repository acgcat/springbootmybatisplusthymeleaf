package com.mxm.controler;

import com.mxm.entity.Student;
import com.mxm.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 漫小猫
 * @since 2019-08-01
 */
@Controller
public class StudentController {
    @Autowired
    IStudentService iStudentService;

    @RequestMapping("/hello1")
    @ResponseBody
    public String hello() {
        Student student = new Student();
        student.setStuName("mxm");
        student.setStuNumber("2535080855");
        student.setAge(21);
        boolean res = iStudentService.insert(student);
        return res ? "success" : "fail";
    }

    @RequestMapping("/hello2")
    @ResponseBody
    public List<Student> hello2() {
        return iStudentService.selectStudentByStuName("mxm");
    }
    @RequestMapping("/hello3")
    @ResponseBody
    public Student hello3() {
        Student su = iStudentService.selectById(1);
        return su;
    }
    @RequestMapping(value = "/hello")
    public String hello(Model model) {
        String name = "漫小猫";
        model.addAttribute("name", name);
        return "hello";
    }
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String name = "漫小猫";
        model.addAttribute("name", name);
        return "index";
    }
}
